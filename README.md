# It's By U Shopify theme

Current Shopify theme build on itsbyu.com

## Dependencies
- Node 6+ https://nodejs.org/en/download/ 
- NPM https://docs.npmjs.com/cli/install

## Local Setup

- Clone repo: `git clone git@bitbucket.org:itsbyu1/itsbyu_shopify_theme.git itsbyu`
- CD into theme dir `cd itsbyu`
- Install Slate globally: `npm install -g @shopify/slate`
- Install the Slate dependencies: `npm install`

The npm installer should run and install all of the build tools we currently use in frontend theme development.

You now have access to all of the Slate [build tools](https://shopify.github.io/slate/commands/)

## Local Development Workflow
Slate allows you to locally build on a dev theme in realtime with live refresh. Full instructions for Slate are here: https://shopify.github.io/slate/

*Note: migration to Slate v1 is pending in late 2018: https://github.com/Shopify/slate/wiki*

Basic setup:

- Duplicate and rename `config-sample.yml` to `config.yml`
- Follow the directions in `config.yml` to obtain an API key and enter in the correct dev theme ID (if not provided by an admin)
- Run `slate watch -e dev` to open a new localhost tab and run SASS/JS realtime compiling
- If you prefer to develop without realtime monitoring/livereload, you have other options outlined here: https://shopify.github.io/slate/commands/#sync-commands

## Git Workflow
There are 2 main branches that are always active:
- `dev`
- `master`

All development should be performed on individual feature branches based off of `dev`. The typical workflow for this is:

- `git checkout dev`
- `git pull`
- `git checkout -b feature-name`
- `git push -u origin feature-x`
- An option for a new PR will be created. Open that or optionally create one via the web admin: https://bitbucket.org/itsbyu1/itsbyu_shopify_theme/pull-requests/new
- Make sure the PR is merging into `dev`
- If you have no need for the branch anymore, remove it when creating the PR
- Submit the PR for review and inclusion

More info on our Git workflow methodology: https://hackernoon.com/still-using-gitflow-what-about-a-simpler-alternative-74aa9a46b9a3

## Dev Themes and Production Theme
All work done on feature branches will directly affect a dev theme on the live store. Additionally, you can perform custom theme customization changes in the theme admin UI (https://help.shopify.com/en/manual/using-themes/change-the-layout/theme-settings) but note all changes made as they are not tracked and can be overwritten by other developers working on the same theme instance.

For minor code changes:
- When all code is tested and merged into `master`, it will then be pushed to the `Live` theme.

For major feature development (including theme customization using the admin UI):
- All work will be tested and approved on the `Dev` theme and then the theme published, replacing `Live`
- The new `Live` theme will then be duplicated and renamed as `Dev`
- **IMPORTANT**: the `dev` theme ID in `config.yml` will then have to be set to the newly created `Dev` theme ID

## Frontend Theme Development
All development files are maintained in the `src` directory. More info: https://help.shopify.com/en/themes/development/templates

The following structure outlines the purpose of each directory:

- **assets**
    - All images (not uploaded in the admin) and directly accessible files should go here. Shopify CDN file URLs for these assets can be used in SASS, Liquid, and JS. 
    - More info: https://help.shopify.com/en/themes/liquid/filters/url-filters
- **config**
    - Default store schema settings such as theme customization settings 
    - More info: https://help.shopify.com/en/themes/development/theme-editor/settings-schema
- **layout** (deprecated)
    - This is no longer used and scheduled for deletion
- **locales**
    - Default language settings for customizable theme areas 
    - More info: https://help.shopify.com/en/themes/development/internationalizing/locale-files
- **scripts**
    - All concatenated JS used in the frontend that's not directly accessible via the `assets` dir
    - To see the load order, open `theme.js.liquid`
    - To add a new JS file into the asset chain:
        - Create the file in the apprpriate dir (or create your own)
        - Add the file to `theme.js.liquid` in this format: `// =require DIR_NAME/FILE_NAME.js`
    - If adding a 3rd party script, place it in the `vendor` dir and add the file to `vendor.js` using the method above.
- **sections**
    - Sections are used to create customizable areas in the theme admin. they are called on specific pages and products.
    - More info: https://help.shopify.com/en/themes/development/sections
- **snippets**
    - Snippets are pieces of code included in other theme files.
    - More info: https://help.shopify.com/en/themes/development/templates#snippets
- **styles**
    - All CSS development is performed here using SASS (https://sass-lang.com/). 
    - To see the load order, open `theme.scss`
    - To create a new file for inclusion in the asset chain:
        - Create the file in the appropriate dir (or create your own)
        - Add your filepath to `theme.scss` in the correct order using the convention: `@import url('DIR_NAME/FILE_NAME.scss');`
    - **global**
        - general load-first styles
    - **modules**
        - section and snippet specific styles
    - **pages**
        - page specific styles
    - **settings**
        - main variable stylesheet
    - **tools**
        - main mixins file
    - **vendor** (largely deprecated)
        - used for 3rd party vendor CSS that's not loaded elsewhere
    - **print.css**
        - used in the occasional need for print capability
    - **theme.css.map**
        - CSS map file for `theme.scss`
    - **theme.scss**
        - Load file for all other scss files
- **templates**
    - Specific page, product, and collection template files
    - More info: https://help.shopify.com/en/themes/development/templates#alternate-templates