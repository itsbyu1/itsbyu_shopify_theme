$(function(){
    var productId, productTitle, deliveryDay, productName;

    $('.sub-product').click(function(){
        productId = $(this).attr('data-product-id');
        productTitle = $(this).attr('data-product-title');
        productName = $(this).attr('data-product-name');
        subscriptionInterval = $(this).attr('data-subscription-interval');
        subscriptionFreq = $(this).attr('data-subscription-frequency');
        subscriptionId = $(this).attr('data-subscription-id');

        $('#redirect .modal-inner .top').html(
            '<span class="text-large"><b>' + productName + ' + Free Starter Kit</b>' + ' added to your cart!</span><br/>'
        );

        $('#redirect').fadeIn(200).css({
            'top':'20%'
        });

        modalHeight();

        var deliveryHeight = $('#delivery-wrapper').height() + $('#deliveryCalc').height();
        console.log(deliveryHeight);

        $('#delivery-date').change(function(){
            deliveryDay = $('#deliveryDay').val(); 
            $('#delivery-wrapper, #deliveryCalc').hide(); 
            $('#loading').show();
            modalHeight();
            addItemToCart(productId , 1, subscriptionFreq, subscriptionInterval, subscriptionId, deliveryDay);
        });

        console.log(productId);

    });

    function modalHeight(){
        $('#redirect').css({'height': $('.modal-inner').outerHeight(true)});
    }

    function addItemToCart (variant_id, quantity, shipping_interval_frequency, shipping_interval_unit_type, subscription_id, delivery_day) {
        data = {
            "quantity": quantity,
            "id": variant_id,
            "properties[shipping_interval_frequency]": shipping_interval_frequency, 
            "properties[shipping_interval_unit_type]": shipping_interval_unit_type,            
            "properties[subscription_id]": subscription_id,
            "properties[First Delivery]": delivery_day
            }
        jQuery.ajax({
            type: 'POST',
            url: '/cart/add.js',
            data: data,
            dataType: 'json'
        })
        .done(function(){
            console.log('Product added');
            //Add starter kit
            $.post('/cart/add.js', { quantity: 1, id: 7049000648737 });
            reChargeProcessCart();
        })
        .fail(function(){
            console.log('Error adding to cart');
            $(this).text('Added to cart!');
            $('#redirect .modal-inner').html('<span class="text-large"><b>Oops! Something went wrong. Please try again.</b>');
            $('#redirect').css({
                'height': $('.modal-inner').outerHeight(true),
                'max-height': '192px'
            });
            setTimeout(function(){
                $('#redirect').css('top','30%').fadeOut(100);
            },1500);
        });
    }

    function reChargeProcessCart() {
        function get_cookie(name){ return( document.cookie.match('(^|; )'+name+'=([^;]*)')||0 )[2] }
        do {
                token=get_cookie('cart');
        }
        while(token == undefined);

        var myshopify_domain='{{ shop.permanent_domain }}'
        try { var ga_linker = ga.getAll()[0].get('linkerParam') } catch(err) { var ga_linker ='' }
        var customer_param = '{% if customer %}customer_id={{customer.id}}&customer_email={{customer.email}}{% endif %}'
        checkout_url= "https://checkout.rechargeapps.com/r/checkout?myshopify_domain="+myshopify_domain+"&cart_token="+token+"&"+ga_linker+"&"+customer_param;
        //window.location.href = checkout_url;
    }
})