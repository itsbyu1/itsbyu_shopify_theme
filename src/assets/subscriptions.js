//Initial product selection
var preSelect = 11211480199; //ID for Every 2 week frequency
var preSelectTile = '.tile[data-id="' + preSelect + '"]';

// 1 month = 78672
// 2weeks = 80615
// everyweek = 80617

//Unfortunately, we need to hardcode the hidden subscription IDs as ReCharge doesn't allow us to fetch them
var intervalUnit = 'Weeks';
var intervalFrequency = '2';
var subscriptionId = '80615';

//Save variant selection
var vIndex, variantId, productId = 0;
$('.variant .tile').on('click', function() {
    vIndex = $(this).attr('data-index');
    variantId = $(this).attr('data-variant');
    if (productId) {
        $('#submit').removeAttr('disabled');
        $('html, body').animate({
            scrollTop: ($('#submit').offset().top)
        }, 500);
    } else {
        $('html, body').animate({
            scrollTop: ($('#frequency').offset().top)
        }, 500);
    }
});

//Reorder by recommended tag
$(function() {
    var middle = $('.middle');
    var closest = $(middle).next('.tile');
    $(middle).insertBefore(closest);
});

//Display variant images depending on which subscription product is chosen
$('#' + preSelect).show();

$('#frequency .tile').on('click', function() {
    productId = '#' + $(this).attr('data-id');
    $('.variant').each(function() {
        $(this).find('.tile').removeClass('selected');
        $(this).hide();
    });

    //Since we're seamlessly swapping out variants in the same position, we need to make sure the same one stay selected and need to set the variantID correctly if frequency is changed.
    var currentVariant = $(productId + ' .tile[data-index="' + vIndex + '"]');
    variantId = currentVariant.attr('data-variant');
    $(currentVariant).addClass('selected');
    $(productId).show();

    //Set recurring product
    if ($(this).is(':contains("Monthly")')) {
        intervalUnit = 'Months';
        intervalFrequency = '1';
        subscriptionId = '78672'
    } else if ($(this).is(':contains("Every week")')) {
        intervalUnit = 'Weeks';
        intervalFrequency = '1';
        subscriptionId = '80617'
    } else {
        intervalUnit = 'Weeks';
        intervalFrequency = '2';
        subscriptionId = '80615';
    }

    if (variantId) {
        $('#submit').removeAttr('disabled');
        $('html, body').animate({
            scrollTop: ($('#submit').offset().top)
        }, 500);
    } else {
        $('html, body').animate({
            scrollTop: ($('#variants').offset().top)
        }, 500);
        $('.success').html('Please choose a style').fadeIn().delay(2000).fadeOut();
    }

});

//Highlight the selection, set the variant ID, and enable cart button
$('.tile').on('click', function() {
    $(this).siblings().removeClass('selected');
    $(this).addClass('selected');

    var price = $('.selected .amount').text();
    $('.bundle-total').html(price);
});

//Progress through steps
$('.next').on('click', function() {
    $(this).add('#frequency').css('opacity', 0).hide();
    $('#submit, .back').show().css('opacity', 1);
});

//Go back to frequency selection
$('.back').on('click', function() {
    $(this).add('#submit').css('opacity', 0).hide();
    $('#frequency, .next').show().css('opacity', 1);
});

$('#addToCartForm').submit(function(e) {
    e.preventDefault();
    var deliveryDay = '';

    jQuery.post('/cart/clear.js');

    var qty = 1;

    if ($('#deliveryDay').val().indexOf('Delivery') === -1) {
        deliveryDay = $('#deliveryDay').val();
        setTimeout(function() {
            addItemToCart(variantId, qty, intervalFrequency, intervalUnit, subscriptionId, deliveryDay)
        }, 800);
    } else {
        $('.delivery-error').remove();
        $('<span class="pink delivery-error" style="font-size:18px; display:block; margin-bottom:1em;">Please select a preferred delivery day</span>').insertBefore('#deliveryDay');
    }

});

function addItemToCart(variant_id, quantity, shipping_interval_frequency, shipping_interval_unit_type, subscription_id, delivery_day) {
    data = {
        "quantity": quantity,
        "id": variant_id,
        "note": 'First delivery: ' + delivery_day,
        "properties[shipping_interval_frequency]": shipping_interval_frequency,
        "properties[shipping_interval_unit_type]": shipping_interval_unit_type,
        "properties[subscription_id]": subscription_id
    }
    if (variantId != 0) {
        jQuery.ajax({
            type: 'POST',
            url: '/cart/add.js',
            data: data,
            dataType: 'json',
            success: function(data) {
                $('.success').html('Product successfully added to cart. Redirecting to checkout...').fadeIn().delay(2000).fadeOut();
                // $('#CartDrawer').load(' #cartContents').addClass('open');
                window.location.href = checkout_url;
            }
        });
    } else {
        console.log('Item not added');
    }
}

// function addToCart(){
//   if (variantId != 0) {
//     $.post('/cart/add.js', {
//       quantity: qty,
//       id: variantId
//     },
//     function(data){
//       $('.success').html('Product successfully added to cart').fadeIn().delay(2000).fadeOut();
//       $('#CartDrawer').load(' #cartContents').addClass('open');
//     },
//     "json");
//   } else {
//     console.log('error');
//   }
// }