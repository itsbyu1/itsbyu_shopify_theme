// Legacy JS brought over from old theme
// TODO: Pare functions down to only necessary

$(function() {

    //Homepage slides
    $("#home-slider").responsiveSlides({
        speed: 800,
        timeout: 6000,
        pause: true,
        auto: true,
        nav: true,
        pager: true,
        nextText: "›",
        prevText: "‹"
    });

    //Popular items slider
    $("#popitems-slider").responsiveSlides({
        speed: 100,
        auto: true,
        nav: true,
        pager: true,
        prevText: "‹",
        nextText: "›"
    });

    // Blog slider
    $("#blog-slider").responsiveSlides({
        speed: 300,
        auto: true,
        nav: true,
        pager: true,
        prevText: "‹",
        nextText: "›"
    });

    //sticky footer
    // function stickyFooter() {
    //   var footerHeight = $('.footer-menu').outerHeight();
    //   $('#PageContainer').css('padding-bottom', footerHeight);
    // }

    // stickyFooter();

    //Lazy load images
    $("img.lazy").show().lazyload({
        threshold: 200,
        effect: "fadeIn",
    });

    //Colorpicker dropdown
    $('.ui-menu').addClass('sponz');

    //if cart drawer is over a certain height, add class for footer box-shadow
    function drawerHeight() {
        if ($('.ajaxcart__inner').height() > 300) {
            $('.ajaxcart__footer').css('box-shadow', '0px -5px 10px #eee');
        }
    }

    //If cart is opened
    $('.js-drawer-open-right').on('click', function() {
        setTimeout(function() {
            drawerHeight();
        }, 1500);
    });

    //check when a product is added to the cart
    $('#AddToCart').on('click', function() {
        setTimeout(function() {
            drawerHeight();
        }, 1500);
    })

    // //promo popup
    // $('#popup-container a.close').click(function() {
    //     $('#popup-container').fadeOut();
    //     $('#active-popup').fadeOut();
    //     $('html').css('overflow', 'visible');
    // });

    // var visits = $.cookie('visits') || 0;
    // visits++;

    // $.cookie('visits', visits, { expires: 1, path: '/' });

    // // console.debug($.cookie('visits'));


    // if ($.cookie('visits') > 1) {
    //     $('#active-popup').hide();
    //     $('#popup-container').hide();
    //     $('html').css('overflow', 'visible');

    // } else {
    //     setTimeout(function() {
    //         var pageHeight = $(document).height();
    //         $('html').css('overflow', 'hidden');
    //         $('<div id="active-popup"></div>').insertBefore('body');
    //         $('#active-popup').css("height", pageHeight);
    //         $('#popup-container').fadeIn(1000);
    //     }, 30000);
    // }

    // if ($.cookie('noShowWelcome')) {
    //     $('#popup-container').hide();
    //     $('#active-popup').hide();
    //     $('html').css('overflow', 'visible');
    // }

    // $(document).mouseup(function(e) {
    //     var container = $('#popup-container');

    //     if (!container.is(e.target) && container.has(e.target).length === 0) {
    //         container.fadeOut();
    //         $('#active-popup').fadeOut();
    //         $('html').css('overflow', 'visible');
    //     }
    // });

    //On tablet/mobile, slightly rearrange product layout
    if ($(window).width() < 769) {
        $('.product-right').insertAfter('#productPhoto');
        $('.product-left').insertBefore('.metafields-left');
    }

    //Get the event date from the cart and add $10 if delivering on a weekend.
    // function weekendUpdate() {

    //   $('#AddToCart').on('click', function(){ 
    //     var orderDate = new Date($('#date').val()); //Create a new dateobject from the date attribute in JSON
    //     var day = orderDate.getDay(); //Get the day from the dateobject
    //     var isWeekend = (day == 1) || (day == 2); //determine day > 1 = Mon, 2 = Tue
    //     if (isWeekend) {
    //       $('.weekend-delivery').show();
    //       CartJS.removeItemById(19884239047);
    //       CartJS.addItem(19884239047,1);
    //     } else {
    //       $('.weekend-delivery').hide();
    //       CartJS.removeItemById(19884239047);
    //     }
    //   });
    // } 

    //Remove no-jes class if js is loaded
    $('body').removeClass('no-js');

    //Continue shopping button
    $('body').on('ajaxCart.afterCartLoad', function(evt, cart) {
        $('#btn-continue').on('click', function(event) {
            timber.RightDrawer.close();

        });

        sendGift();
    });

    // Fetch the cart in JSON and change cart quantity on the fly after first product added to cart
    function doPoll() {
        $.getJSON('/cart.js', function(cart) {
            $('.cart-count').html(cart.item_count);
            if (cart.attributes.date != null) {
                $('.event-date').html('Day of Event: ' + cart.attributes.date);
            }
        });
    }

    // $( document ).ajaxComplete(function() {
    //   doPoll();
    // });


    //Open social login modal
    $('#customer_login_link').on('click', function() {
        Hull.emit('hull.login.showDialog', options);
    });

    setTimeout(function() {
        $('#create_customer .ship-56e83aa35926d8962700018b').remove();
    }, 1000);

    //Hide account form when forgot password link is clicked
    $('#RecoverPassword').on('click', function() {
        $('#RecoverPasswordForm').fadeIn(200);
        $(window).scrollTop(0);
    });

    sendGift();

}); //end docready

//Email validator
function isEmail(emailV) {
    if (emailV != null && emailV != undefined) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailV);
    } else {
        return false;
    }

}

function sendGift() {
    //Check if gift input is selected
    var checkout = '.cart__checkout';

    $('#yes-gift').change(function() {
        $(this).each(function() {
            $(this).prop('checked', this.checked)
        });

        $('.gift-email').toggleClass('checked');
        $(checkout).toggleClass('disabled');
        if ($(checkout).hasClass('disabled')) {
            $(checkout).attr('disabled', 'disabled');
            $('.gift-email').attr('required');
        } else {
            $(checkout).removeAttr('disabled', 'disabled');
        }

    });

    $('.gift-email').on('change', function() {
        if (isEmail($(this).val())) {
            $(checkout).removeClass('disabled').removeAttr('disabled', 'disabled');
            $('.valid-email').hide();
        } else {
            $(checkout).addClass('disabled').attr('disabled', 'disabled');
            $('.valid-email').show();
        }
    });

}


//Fix header to top on scroll
$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 63) {
        $('.nav-bar').addClass("fixed");
    } else {
        $('.nav-bar').removeClass("fixed");
    }

});

$(function() {
    $('.alert').on('click', function() {
        $('html, body').animate({
            scrollTop: $('#mc_embed_signup').offset().top
        }, 200);
    });
})