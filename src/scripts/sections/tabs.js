$(document).ready(function() {

    if ($(window).width() > 767) {
        var currentPath = window.location.href;
        var tabRegex = /#tab-([0-9]+)$/;

        if ($('.tabs').length) {

            if (!currentPath.match(tabRegex)) {
                $('#tab-1').show();
            }
        }
    }

});