$(document).ready(function(){

  // Add a 'js' class to the html tag
  // If you're using modernizr or similar, you
  // won't need to do this
  $('html').addClass('js');
  $('.video-overlay').show();
  // Fade in videos
  var $fade_in_videos = $('.video-bg video');
  $fade_in_videos.each(function(){
    if( $(this)[0].currentTime > 0 ) {
      // It's already started playing
      $(this).addClass('is-playing');
    } else {
      // It hasn't started yet, wait for the playing event
      $(this).on('playing', function(){
        $(this).addClass('is-playing');
      });
    }
  });

  // Hide video section on mobile
  // var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/.test(navigator.platform) || /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/.test(navigator.userAgent);
  // if( mobile ) {
  //   $('.main-carousel-mobile video').remove();
  //   $('.video-bg .video-placeholder').show();
  // }

});
