$(function() {
    function getDelivery(){
      var today = new Date();
      var saturday = today.setDate(today.getDate() + (6 + 7 - today.getDay()) % 7);
      var deliveryDay = '';
      var chosenDay = $('.delivery-option--active').attr('data-note');
      if (chosenDay == 'Thursday'){
        if (new Date() < saturday){
        deliveryDay = new Date(today.setDate(today.getDate() + (4 + 7 - today.getDay()) % 7));
        formatDate();
      } else {
        deliveryDay = new Date(today.setDate(today.getDate() + (4 + 14 - today.getDay()) % 14));
        formatDate();
      }
    } else if (chosenDay == 'Friday'){
      if (new Date() < saturday){
        deliveryDay = new Date(today.setDate(today.getDate() + (5 + 7 - today.getDay()) % 7));
        formatDate();
      } else {
        deliveryDay = new Date(today.setDate(today.getDate() + (5 + 14 - today.getDay()) % 14));
        formatDate();
      }
    }

    function formatDate(){
      var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
      ];

      var month = deliveryDay.getMonth();
      var day_num = deliveryDay.getDate();
      var day = ordinal_suffix_of(day_num)
      return $('.date').html(chosenDay + ', ' + monthNames[month] + ' ' + day)
    }

    // Calculate suffix for day (1st, 2nd 3rd, 4th)
    function ordinal_suffix_of(i) {
      var j = i % 10,
          k = i % 100;
      if (j == 1 && k != 11) {
          return i + "st";
      }
      if (j == 2 && k != 12) {
          return i + "nd";
      }
      if (j == 3 && k != 13) {
          return i + "rd";
      }
      return i + "th";
    }
  } getDelivery();

  // Choose delivery date toggle (subscription template, single-product template)
  $('.delivery-option').click(function(){
    if ($(this).hasClass('delivery-option--active')){
      return;
    } else {
        $('.delivery-option').removeClass('delivery-option--active');
        $(this).addClass('delivery-option--active');
        getDelivery();
      };
  });
});
