/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */

theme.Product = (function() {

    var selectors = {
        addToCart: '[data-add-to-cart]',
        addToCartText: '[data-add-to-cart-text]',
        comparePrice: '[data-compare-price]',
        comparePriceText: '[data-compare-text]',
        originalSelectorId: '[data-product-select]',
        priceWrapper: '[data-price-wrapper]',
        productFeaturedImage: '[data-product-featured-image]',
        productJson: '[data-product-json]',
        productPrice: '[data-product-price]',
        productThumbs: '[data-product-single-thumbnail]',
        singleOptionSelector: '[data-single-option-selector]'
    };

    /**
     * Product section constructor. Runs on page load as well as Theme Editor
     * `section:load` events.
     * @param {string} container - selector for the section container DOM element
     */
    function Product(container) {
        this.$container = $(container);
        var sectionId = this.$container.attr('data-section-id');

        this.settings = {};
        this.namespace = '.product';

        // Stop parsing if we don't have the product json script tag when loading
        // section in the Theme Editor
        if (!$(selectors.productJson, this.$container).html()) {
            return;
        }

        this.productSingleObject = JSON.parse($(selectors.productJson, this.$container).html());
        this.settings.imageSize = slate.Image.imageSize($(selectors.productFeaturedImage, this.$container).attr('src'));

        slate.Image.preload(this.productSingleObject.images, this.settings.imageSize);

        this.initVariants();
    }

    Product.prototype = $.extend({}, Product.prototype, {

        /**
         * Handles change events from the variant inputs
         */
        initVariants: function() {
            var options = {
                $container: this.$container,
                enableHistoryState: this.$container.data('enable-history-state') || false,
                singleOptionSelector: selectors.singleOptionSelector,
                originalSelectorId: selectors.originalSelectorId,
                product: this.productSingleObject
            };

            this.variants = new slate.Variants(options);

            this.$container.on('variantChange' + this.namespace, this.updateAddToCartState.bind(this));
            this.$container.on('variantImageChange' + this.namespace, this.updateProductImage.bind(this));
            this.$container.on('variantPriceChange' + this.namespace, this.updateProductPrices.bind(this));
        },

        /**
         * Updates the DOM state of the add to cart button
         *
         * @param {boolean} enabled - Decides whether cart is enabled or disabled
         * @param {string} text - Updates the text notification content of the cart
         */
        updateAddToCartState: function(evt) {
            var variant = evt.variant;

            if (variant) {
                $(selectors.priceWrapper, this.$container).removeClass('hide');
            } else {
                $(selectors.addToCart, this.$container).prop('disabled', true);
                $(selectors.addToCartText, this.$container).html(theme.strings.unavailable);
                $(selectors.priceWrapper, this.$container).addClass('hide');
                return;
            }

            if (variant.available) {
                $(selectors.addToCart, this.$container).prop('disabled', false);
                $(selectors.addToCartText, this.$container).html(theme.strings.addToCart);
            } else {
                $(selectors.addToCart, this.$container).prop('disabled', true);
                $(selectors.addToCartText, this.$container).html(theme.strings.soldOut);
            }
        },

        /**
         * Updates the DOM with specified prices
         *
         * @param {string} productPrice - The current price of the product
         * @param {string} comparePrice - The original price of the product
         */
        updateProductPrices: function(evt) {
            var variant = evt.variant;
            var $comparePrice = $(selectors.comparePrice, this.$container);
            var $compareEls = $comparePrice.add(selectors.comparePriceText, this.$container);

            $(selectors.productPrice, this.$container)
                .html(slate.Currency.formatMoney(variant.price, theme.moneyFormat));

            if (variant.compare_at_price > variant.price) {
                $comparePrice.html(slate.Currency.formatMoney(variant.compare_at_price, theme.moneyFormat));
                $compareEls.removeClass('hide');
            } else {
                $comparePrice.html('');
                $compareEls.addClass('hide');
            }
        },

        /**
         * Updates the DOM with the specified image URL
         *
         * @param {string} src - Image src URL
         */
        updateProductImage: function(evt) {
            var variant = evt.variant;
            var sizedImgUrl = slate.Image.getSizedImageUrl(variant.featured_image.src, this.settings.imageSize);

            $(selectors.productFeaturedImage, this.$container).attr('src', sizedImgUrl);
        },

        /**
         * Event callback for Theme Editor `section:unload` event
         */
        onUnload: function() {
            this.$container.off(this.namespace);
        }
    });

    return Product;
})();

$(document).ready(function() {

    $('.variant').click(function() {
        var price = $(this).attr('data-price');

        $(this).siblings().removeClass('selected');
        $(this).addClass('selected');

        var variantId = $(this).attr('data-variant-id');
        $('#productSelect').val(variantId);

        updatePrice();

        setTimeout(function() {
            $('.gwbutton #AddToCartText').text('Customize your gift card');
            $('.gwbutton #AddToCart').removeAttr('disabled', 'disabled');
            $('.gwbutton').css('margin-left', 'auto').show();
            $('.gwbutton #AddToCart').removeClass('disabled').css('opacity', 1);
            $('body, html').animate({
                scrollTop: ($('.gwbutton').offset().top - 200)
            }, 200);
        }, 400);
    });

    $(document).on('click', '.gwbutton', function() {
        $('body, html').animate({
            scrollTop: ($('#page').offset().top)
        }, 400);
        $('.loading').css('display', 'flex');
        setTimeout(function() {
            $('.loading').hide();
        }, 2000);
    });

    $('.quantity-selector .icon').on('click', function() {
        updatePrice();
    });

    function updatePrice() {
        var freqPrice = $('.selected').attr('data-price');
        var qty = $('#quantity').val();
        var currentPrice = freqPrice * qty;

        $('.price').text(freqPrice);
    }

    //Additional fixes for delivery date field
    $(document).click('.input-group.date', function(){
        setTimeout(function(){
            $('.date .input-group-addon').triggerHandler('click');
        },100);
    });

    setTimeout(function(){
        if ($(window).width() < 769) {
            $('#delivery-wrapper').insertAfter('h1.uc');
        }
    },400);

    $(window).on('load resize', function(){
        productHeaderPlacement();
        headerHeight();
        $('#productHeaderBg .float').each(function(){
            $(this).show();
        })
    });
    

    $(document).on('click', '#gift-prompt', function(){
        $('.form-field-wrapper.submit, #prepaidSelect').toggleClass('on');
        $(this).toggleClass('on btn-red');
        if ($(this).hasClass('on')){
            $(this).text('Cancel');
        } else {
            $(this).text('Gift this!');
        }
    });

    function productHeaderPlacement() {
        if ($(window).width() < 768) {
            $('#productForm').insertBefore('#productInfo');
        } else {
            $('#productForm').appendTo('#productHeaderBg .float:last-child');
        }
    }
    
    var cachedWidth = $(window).width();
    $(window).resize(function(){
        var newWidth = $(window).width();
        if(newWidth !== cachedWidth){
            headerHeight();
            cachedWidth = newWidth;
        }
    });

    function headerHeight() {
        var headHeight = $('#productHeader').height();

        if ($(window).width() > 767) {
            $('#productHeaderBg').css({
                'border-left': '600rem solid #F3F3F3',
                'border-right': '600rem solid #F3F3F3',
                'background': '#F3F3F3',
                'margin': '0 -601.5rem'
            });
            setTimeout(function(){
                $('#productHeaderBg').height(headHeight);
            },100);
        }
    }
});