// Extends QS Datepicker: https://github.com/qodesmith/datepicker

// Set start date to closest Thursday with cutoff at Sat midnight GMT-5
let firstDay = new Date();
let currentTime = new Date();
let num = currentTime.getDate();

firstDay.setDate(num);
currentTime.setDate(num);

currentTime.setHours(0, 0, 0, 0);

//Find closest upcoming Thursday (midnight)
firstDay.setHours(0, 0, 0, 0);

const fiveDays = 86400000 * 5;
const currentDiff = firstDay - currentTime;

if (currentDiff < fiveDays) {
  firstDay.setDate(firstDay.getDate() + ((4 + 7 - firstDay.getDay()) % 7) + 7);
} else {
  firstDay.setDate(firstDay.getDate() + ((4 + 7 - firstDay.getDay()) % 7));
}

// All configurable options
const dateOptions = {
  // Customizations.
  formatter: function (input, date, instance) {
    var options = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    };
    const value = date.toLocaleDateString('en-US', options);
    input.value = value;
  },
  position: 'tr', // Top right.
  startDay: 1, // Calendar week starts on a Monday.
  customDays: ['S', 'M', 'T', 'W', 'Th', 'F', 'S'],
  customMonths: [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ],

  // Settings.
  minDate: firstDay,
  startDate: firstDay, // This month.

  // Disabling things.
  noWeekends: true, // Saturday's and Sunday's will be unselectable.
  disabler: date => date.getDay() < 4, // Disable every weekday other than Thursday and Friday
  disableYearOverlay: true, // Clicking the year or month will *not* bring up the year overlay.
};

if ($("#datepicker").length) {
  const picker = datepicker(document.querySelector("#datepicker"), dateOptions);

  $("#datepicker").focus(function(e) {
    $(this).blur();
    picker.show();
  });

  $("#cal-icon, #cal-arrow").click(function() {
    setTimeout(function() {
      picker.show();
    }, 100);
  });
}

$(document).ready(function() {
  $("#datepicker")
    .parents(".delivery-inner")
    .addClass("input-wrap");
});
