$(function() {
  $('.product__dropdown').click(function(){
    if ($(this).hasClass('active')){
      $(this).removeClass('active');
      $(this).find('.toggle-down').show();
      $(this).find('.toggle-up').hide();
      $(this).next('.product__dropdown--hidden').slideUp();
      console.log('active')
    } else {
      $('.product__dropdown--hidden').slideUp();
      $('.product__dropdown').removeClass('active');
      $('.toggle-down').show();
      $('.toggle-up').hide();
      $(this).addClass('active');
      $(this).find('.toggle-down').hide();
      $(this).find('.toggle-up').show();
      $(this).next('.product__dropdown--hidden').slideDown();
    }
  });
});
