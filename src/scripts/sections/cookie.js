$(document).ready(function () {

    function setCookie(cname, cvalue, hours) {
        var d = new Date();
        d.setTime(d.getTime() + (hours * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function checkCookie() {
        const checkCookie = document.cookie.indexOf('delivery_date') == -1;
        if (checkCookie) {
            // Clear cart and saved attributes
            CartJS.setAttribute('First Delivery', null);
            CartJS.setAttribute('Preferred Delivery Day', null);
            $.post('/cart/clear', function (data) {
                console.log('Cart cleared');
            });
            setCookie("delivery_date", "ddate", 1);
        } else {
            console.log('Cart stable');
        }
    }

    checkCookie();

});