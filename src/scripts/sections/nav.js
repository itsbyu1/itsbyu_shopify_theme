// Dropdown submenu
  $(function () {
    $(".nav-desktop").mouseenter(function () {
        $(".nav-dropdown-desktop").slideDown('medium');
    });
    $(".nav-desktop").mouseleave(function () {
        $(".nav-dropdown-desktop").slideUp('medium');
    });
    $('.nav-dropdown-desktop').hover(function () {
      $(this).stop();
     }, function () {
       $(this).slideUp('medium');
     });

     $(".nav-mobile").click(function () {
         $(".nav-dropdown-mobile").slideToggle('medium');
     });     
  });
