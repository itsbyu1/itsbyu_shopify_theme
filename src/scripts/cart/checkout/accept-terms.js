$(function() {
  $(".terms-inner").click(function() {
    if ($("#agree").is(":checked")) {
      $("#agree").prop("checked", false);
    } else {
      $("#agree").prop("checked", true);
    }
  });
});
