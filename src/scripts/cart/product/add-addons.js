//AJAX add addons to cart
$(function() {
  $(".product-addons__item").on("click", function() {
    var id = $(this).attr("data-id");
    var el = $(this);
    $(el).addClass("disabled");
    if ($(this).hasClass("active")) {
      CartJS.updateItemById(
        id,
        0,
        {},
        {
          success: function(data, textStatus, jqXHR) {
            $(el)
              .find(".product-addons__item-add span")
              .text("Add to Purchase");
            $(el)
              .removeClass("disabled")
              .removeClass("active");
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + "!");
          }
        }
      );
    } else {
      CartJS.addItem(
        id,
        1,
        {},
        {
          success: function(data, textStatus, jqXHR) {
            $(el)
              .find(".product-addons__item-add span")
              .text("Added!");
            $(el)
              .removeClass("disabled")
              .addClass("active");
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + "!");
          }
        }
      );
    }
  });
});
