$(document).on("click", ".cart-quantity", function() {
  var subExists = $(".isSub").length;
  var vaseExists = $(".cart-quantity").hasClass("isVase");
  if (cartDisabled == true) {
    return;
  } else {
    cartDisabled = true;
    var self = $(this);
    var variant_index = $(self).attr("data-index");
    if ($(this).hasClass("cart-quantity-plus")) {
      var cartQuantity = $(self)
        .prev(".cart-quantity-label")
        .attr("data-value");
      cartQuantity = ++cartQuantity;

      CartJS.updateItem(
        variant_index,
        cartQuantity,
        {},
        {
          success: function(data, textStatus, jqXHR) {
            // get the quantity displayed in menu cart/basket icon
            var cart_basket_quantity = $(".cart-basket-quantity").html();
            $(".cart-basket-quantity").html(
              parseInt(cart_basket_quantity) + parseInt(1)
            );
            $(self)
              .prev(".cart-quantity-label")
              .html(cartQuantity);
            $(self)
              .prev(".cart-quantity-label")
              .attr("data-value", cartQuantity);
            cartDisabled = false;
            window.location.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            window.location.reload();
          }
        }
      );
    } else {
      var cartQuantity = $(self)
        .next(".cart-quantity-label")
        .attr("data-value");
      cartQuantity = --cartQuantity;

      if (subExists == 1 && cartQuantity == 0 && vaseExists) {
        console.log("true");
        removeVase = true;
      }

      CartJS.updateItem(
        variant_index,
        cartQuantity,
        {},
        {
          success: function(data, textStatus, jqXHR) {
            if (removeVase == true) {
              CartJS.removeItemById($(".isVase").attr("data-variant"), {
                success: function(data, textStatus, jqXHR) {
                  window.location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                  console.log("Error: " + errorThrown + "!");
                  window.location.reload();
                }
              });
            }

            var cart_basket_quantity = $(".cart-basket-quantity").html();
            $(".cart-basket-quantity").html(
              parseInt(cart_basket_quantity) - parseInt(1)
            );
            $(self)
              .next(".cart-quantity-label")
              .html(cartQuantity);
            $(self)
              .next(".cart-quantity-label")
              .attr("data-value", cartQuantity);
            cartDisabled = false;
            window.location.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + "!");
            window.location.reload();
          }
        }
      );
    }
  }
});
