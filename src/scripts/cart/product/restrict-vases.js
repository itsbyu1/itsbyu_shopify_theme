//Check to ensure vases can't be purchased without subscription
$(function() {
  $.ajax({
    url: "/cart.js",
    dataType: "json",
    success: function(data) {
      let hasSub = false;
      let hasVase = false;
      data.items.forEach(function(item) {
        if (item.properties) {
          if (
            item.properties.type === "subscription" ||
            item.properties.type === "gift-subscription"
          ) {
            hasSub = true;
          }
        }
      });
      if (hasSub === false) {
        let vases = [
          12741865242647,
          12744399454231,
          12744402731031,
          12744388575255
        ];
        vases.forEach(function(id) {
          CartJS.updateItemById(id, 0);
          $('tr[data-variant-id="' + id + '"').remove();
        });
      }
    }
  });
});
