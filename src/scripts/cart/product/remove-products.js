$(function() {
  // Remove products
  var cartDisabled = false;
  var removeVase = false;
  $(document).on("click", ".cart-remove", function() {
    // get the quantity displayed in menu cart/basket icon
    var cart_basket_quantity = $(".cart-basket-quantity").html();
    // reload #CartDrawer DOM, or else drawer will pop open and still remain empty
    var url = window.location + "?ID=" + Math.random();
    var cart = CartJS.cart.items;
    var remove_item = $(this).attr("data-variant");
    var addon_type = $(this).data("type");
    var self = $(this);
    var vase_id = self.attr("data-product-id");
    var vaseExists = $(".cart-quantity").hasClass("isVase");
    var giftExists = $(".isGift").length;
    var subExists = $(".isSub").length;
    // var kit_quantity = $('.kit-info').attr('data-value');
    // var kit_index = $('.kit-info').attr('data-index');
    // var kit_variant = $('.kit-info').attr('data-variant');

    if (cartDisabled == true) {
      return;
    } else {
      cartDisabled = true;
      CartJS.removeItemById(remove_item, {
        success: function(data, textStatus, jqXHR) {
          console.log("removing");
          if (
            (vaseExists && giftExists == 1) ||
            (vaseExists && subExists == 1)
          ) {
            CartJS.removeItemById($(".isVase").attr("data-variant"), {
              success: function(data, textStatus, jqXHR) {
                window.location.reload();
              },
              error: function(jqXHR, textStatus, errorThrown) {
                console.log("Error: " + errorThrown + "!");
                window.location.reload();
              }
            });
          }
          window.location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log("Error: " + errorThrown + "!");
          window.location.reload();
        }
      });
    }
  });
});
