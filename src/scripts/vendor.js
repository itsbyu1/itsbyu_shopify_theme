/*!
 * modernizr.min.js
 */
// =require vendor/modernizr.min.js

/* Responsive slides */
// =require vendor/responsive-slides.min.js
// =require vendor/bglazyload.js

/* Resp slides (LEGACY) */
// =require vendor/respSlides.js

/* Lazyload images (LEGACY) */
// =require vendor/lazyload.js

// =require vendor/cartjs.js
// =require vendor/handlebars.min.js
// =require vendor/jquery.cookie.js
// =require vendor/svg-convert.js
// =require vendor/jquery.selectric.min.js

// Attempts to preserve comments that likely contain licensing information,
// even if the comment does not have directives such as `@license` or `/*!`.
//
// Implemented via the [`uglify-save-license`](https://github.com/shinnn/uglify-save-license)
// module, this option preserves a comment if one of the following is true:
//
// 1. The comment is in the *first* line of a file
// 2. A regular expression matches the string of the comment.
//    For example: `MIT`, `@license`, or `Copyright`.
// 3. There is a comment at the *previous* line, and it matches 1, 2, or 3.